export const get_users_resquest = "USERS/GET_USERS";
export const get_users_succeed = "USERS/GET_USERS_SUCCEED";
export const get_users_failed = "USERS/GET_USERS__FAILED";


export const getUser_action = () => {
  return {
    type: get_users_resquest,
  };
};

export const get_user_succeed_action = (users) => {
  return {
    type: get_users_succeed,
    payload: users,

  };
};

export const get_user_failed_action = (errorMessage) => {
  return {
    type: get_users_failed,
    payload: errorMessage,
  };
};
