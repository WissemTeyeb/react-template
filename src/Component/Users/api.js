import axios from "axios";
import { put, takeLatest, call } from "redux-saga/effects";
import * as actions from "./actions";


//generator function which helps us to write asynchronous code.

/** function that returns an axios call */
function getUsersApi() {
  return axios.request({
    method: "get",
    url: "/url/api/v1/users",
  });
}

/** saga worker that is responsible for the side effects */
function* getUsersEffectSaga(action) {
  try {
    // data is obtained after axios call is resolved
    let { data } = yield call(getUsersApi);
    // dispatch action to change redux state
    yield put(actions.get_user_succeed_action(data));

  } catch (e) {
    // catch error on
    //yield put(actions.test_saga_failed_action(e.message));
    // alert using an alert library
  }
}

export function* usersWatcher() {
  yield takeLatest(actions.get_users_resquest, getUsersEffectSaga);
  yield takeLatest("@@router/LOCATION_CHANGE", getUsersEffectSaga);

}
