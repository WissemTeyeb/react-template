export function usersReducer(state = { users: [], message: "", requestState: "NotRequested" }, action) {
  switch (action.type) {
    case "USERS/SIGNIN_LOADING":
      return { ...state, requestState: "Loading" };

    case "USERS/GET_USERS__FAILED":
      return { ...state, message: action.payload, requestState: "NOk" };
    case "USERS/GET_USERS_SUCCEED":
      return { ...state, users: action.payload, requestState: "Ok" };

    default:
      return state;
  }
}
