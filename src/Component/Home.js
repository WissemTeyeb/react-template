import React from "react";
import SagaComponent from "./Redux-saga-component/Redux-saga-component";
import ThunkComponet from "./Redux-thunk-component/Redux-thunk-component";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import { signOut_action } from "./SignIn/actions";
import { getUser_action } from "./Users/actions";

import { connect } from "react-redux";
import { Button } from '@material-ui/core';
import UsersTable from './Users/userTable';
function mapStateToProps(state) {
  const { todos } = state
  return { users: state.users.users }
}

const mapDispatchToProps = (dispatch) => {
  return {
    logOut: () => {
      dispatch(signOut_action());
    },
    getusers: () => {
      dispatch(getUser_action());
    },
  };
};
const Home = ({ logOut, users, getusers }) => {

  const [count, setCount] = React.useState(0)

  React.useEffect(() => {
    console.log(count)
    return () => { //on utilise souvent return function pour faire un clear pour tou démontage
      console.log("by wissem")
    }
  }, [count])


  return (
    <Box sx={{ flexGrow: 10 }}>
      <Grid container spacing={50}>
        <Grid item xs={12}>
          <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
              <Toolbar>
                <IconButton
                  size="large"
                  edge="start"
                  color="inherit"
                  aria-label="menu"
                  sx={{ mr: 2 }}
                >
                  <MenuIcon />
                </IconButton>
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                  Welcome
                </Typography>
                <Button color="inherit" onClick={logOut}>
                  Logout
                </Button>


              </Toolbar>
            </AppBar>
          </Box>
        </Grid>

        <Grid item xs={2}>
          <SagaComponent />
        </Grid>
        <Grid item xs={1}>
          <ThunkComponet />
        </Grid>

        <Grid item xs={1}>

          <Button variant="contained">
            test Hooks
          </Button>
        </Grid>

        <Grid item xs={1}>

          <Button variant="contained" onClick={getusers}>
            Get Users
          </Button>
        </Grid>
        <Grid item xs={12}>
          <UsersTable users={users} />
        </Grid>

      </Grid>
    </Box>
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(Home);
