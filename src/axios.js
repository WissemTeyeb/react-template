
import Axios from "axios";

const cancelTokenSource = Axios.CancelToken.source();

/** function that returns an axios call */
export function post(authParams) {
    return Axios.request({
        method: "post",
        url: "/url/api/auth/signin",
        data: authParams,
        cancelToken: cancelTokenSource.token

    });
}

Axios.interceptors.response.use(undefined, function (error) {
    // Error Status code

    // If the un-auth error, we should redirect to login page.
    const statusCode = error.response ? error.response.status : null;
    if (statusCode === 403) {
        alert("login Failed ! Please verify your Credentials")
    }


    // return Promise.reject(error);
});